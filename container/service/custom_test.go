package service

import (
	"testing"
	"encoding/json"
)

func TestNewAirConditioner(t *testing.T) {
	airConditioner := NewAirConditioner()
	data, _ := json.Marshal(airConditioner.Service)
	t.Log(string(data))
}

func TestNewClock(t *testing.T) {
	clock := NewClock()
	data, _ := json.Marshal(clock.Service)
	t.Log(string(data))
}

func TestNewHTSensor(t *testing.T) {
	ht := NewHTSensor()
	data, _ := json.Marshal(ht.Service)
	t.Log(string(data))
}

func TestNewLightSensor(t *testing.T) {
	light := NewLightSensor()
	data, _ := json.Marshal(light.Service)
	t.Log(string(data))
}

func TestNewContactSensor(t *testing.T) {
	contact := NewContactSensor()
	data, _ := json.Marshal(contact.Service)
	t.Log(string(data))
}

func TestNewSwitch(t *testing.T) {
	sw := NewSwitch()
	data, _ := json.Marshal(sw.Service)
	t.Log(string(data))
}

func TestNewOutlet(t *testing.T) {
	outlet := NewOutlet()
	data, _ := json.Marshal(outlet.Service)
	t.Log(string(data))
}


