package accessory

type Type int

const (
	TypeHomeMaster            Type = 1
	TypeHomeMasterV2          Type = 2
	TypeOutlet                Type = 3
	TypeSwitch                Type = 4
	TypeContactSensor         Type = 6
	TypeHTSensor              Type = 7
	TypeColorLight            Type = 8
	TypeTemperatureColorLight Type = 9
	TypeMotionSensor          Type = 10
)
