//
// +build !mipsle

package modules

// 驱动设备类型
const (
	// 温湿度计
	MasterHTSensor = 0x01
	// 光线传感器
	MasterLightSensor = 0x02
	// OLED 屏幕
	MasterOled = 0x11
	// 433 射频
	MasterRadio = 0x21
	// 红外
	MasterInfrared = 0x22
)

const MasterMessageMaxLen = 128
const MasterDevPath  = "build/tmp/master"
