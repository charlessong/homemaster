package coordinator

import "math"


// philips hue go
// 0xE9D9
// 001788010116B471

// philips hue
// 07F6
// 0017880103A0F87E

// CIE xy
func rgbToCIEXY(r, g, b uint8) (x, y uint16) {
	cred := float64(r) / 255
	cgreen := float64(g) / 255
	cblue := float64(b) / 255

	//log.Printf("red: %04f green: %04f blue: %04f\n", cred, cgreen, cblue)
	var red, green, blue float64
	if cred > 0.04045 {
		red = math.Pow((cred + 0.055) / (1.0 + 0.055), 2.4)
	} else {
		red = cred / 12.92
	}
	if cgreen > 0.04045 {
		green = math.Pow((cgreen + 0.055) / (1.0 + 0.055), 2.4)
	} else {
		green = cgreen / 12.92
	}
	if cblue > 0.04045 {
		blue = math.Pow((cblue + 0.055) / (1.0 + 0.055), 2.4)
	} else {
		blue = cblue / 12.92
	}

	//log.Printf("red: %04f green: %04f blue: %04f\n", red, green, blue)

	X := red * 0.649926 + green * 0.103455 + blue * 0.197109
	Y := red * 0.234327 + green * 0.743075 + blue * 0.022598
	Z := red * 0.0000000 + green * 0.053077 + blue * 1.035763

	fx := X / (X + Y + Z)
	fy := Y / (X + Y + Z)

	x = uint16(fx * 0xffff)
	y = uint16(fy * 0xffff)
	return
}
